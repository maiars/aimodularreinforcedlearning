﻿using System;
using System.Collections.Generic;
using System.Linq;
using AI.Provider;
using Feed;

namespace AI
{
    /// <summary>
    /// Handles AI
    /// </summary>
    public class Handle
    {
        /// <summary>
        /// Initiates the AI
        /// </summary>
        public Handle()
        {
            frameData = new List<FrameData>();
        }

        /// <summary>
        /// Stores the FrameData object to finally store it
        /// </summary>
        private readonly List<FrameData> frameData;

        /// <summary>
        /// Chooses an action, could be the best action, it could be trying something new or it could be defining the best
        /// </summary>
        /// <param name="data"></param>
        public Enums.Action Action(FrameData data)
        {
            var bestAction = Best(data);

            // Update local framedata
            data.Action = bestAction;
            frameData.Add(data);

            return bestAction;
        }

        /// <summary>
        /// Iteration has ended, store it with the outcome value
        /// </summary>
        /// <param name="outcome"></param>
        public void IterationEnd(Enums.Outcome outcome)
        {
            Store(outcome);
        }

        /// <summary>
        /// All possibilities
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static IEnumerable<Enums.Action> Possibilities(FrameData data)
        {
            var possibilities = new List<Enums.Action>();

            possibilities.Add(Enums.Action.Idle);

            if (data.CanMoveUp)
                possibilities.Add(Enums.Action.MoveUp);

            if (data.CanMoveDown)
                possibilities.Add(Enums.Action.MoveDown);

            if (data.CanYouShoot)
                possibilities.Add(Enums.Action.Shoot);

            if (data.BombAvailable)
                possibilities.Add(Enums.Action.Bomb);

            return possibilities.ToArray();
        }

        /// <summary>
        /// Best action the AI can take, it will try new actions to experience, or will define best
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static Enums.Action Best(FrameData data)
        {
            //Retrieve known possibilities on the DB, unordered
            var knownPossibilities = Experience.Get(data);

            //Retrieve every possibility from the Game
            var possibilities = Possibilities(data);

            //Do I know the outcome of every action? If not, try something new
            foreach (var item in possibilities)
            {
                if (!knownPossibilities.Exists(x => x.Action == item))
                {
                    //I found a possibility I haven't tried yet, so I will try it
                    return item;
                }
            }

            //Testing Threshold: There is a possibility which I'm still not sure if it's the best one
            //Order the possibilites and select something from the ones that are inside the threshold
            var bestValue = knownPossibilities.Max(x => x.Value);
            knownPossibilities = knownPossibilities.FindAll(x =>
            {
                if (x.Value <= bestValue && x.Value > bestValue - Constants.Threshold)
                {
                    return true;
                }
                return false;
            });

            //Randomize the list
            knownPossibilities.Shuffle();

            //I know all possibilities, I will try the best outcome
            return knownPossibilities[0].Action;
        }

        /// <summary>
        /// Stores what has been learned
        /// </summary>
        /// <param name="outcome"></param>
        private void Store(Enums.Outcome outcome)
        {
            int value;

            switch (outcome)
            {
                case Enums.Outcome.Good:
                    value = 1;
                    break;
                case Enums.Outcome.Neutral:
                    value = 0;
                    break;
                case Enums.Outcome.Bad:
                    value = -1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            var distinct = frameData.Distinct().ToArray();

            Experience.Update(distinct, value);
        }
    }
}