﻿using System.Collections.Generic;
using System.Security.Cryptography;
using Feed;

namespace AI
{
    public static class Utilities
    {

        /// <summary>
        /// Randomizes a list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        public static void Shuffle<T>(this IList<T> list)
        {
            var provider = new RNGCryptoServiceProvider();
            var elements = list.Count;
            while (elements > 1)
            {
                var box = new byte[1];
                do provider.GetBytes(box);
                while (!(box[0] < elements * (byte.MaxValue / elements)));
                var randomElement = box[0] % elements;
                elements--;
                var value = list[randomElement];
                list[randomElement] = list[elements];
                list[elements] = value;
            }
        }        
        /// <summary>
        /// Creates a readable and formated string from a point array
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static string ToCustomString(this Point[] points)
        {
            var result = string.Empty;
            for (var i = 0; i < points.Length; i++)
            {
                var point = points[i];
                result += point.X + "," + point.Y;
                if (i != points.Length - 1)
                {
                    result += "|";
                }
            }

            return result;
        }
    }
}
