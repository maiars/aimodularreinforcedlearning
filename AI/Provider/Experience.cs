﻿using System.Collections.Generic;
using MrProvider;
using Feed;

namespace AI.Provider
{
    /// <summary>
    /// Handles experience data
    /// </summary>
    public static class Experience
    {
        /// <summary>
        /// Retrieves what the AI knows about this situacion
        /// </summary>
        /// <param name="data"></param>
        public static List<Choice> Get(FrameData data)
        {
            var db = new Connector("dbo.GetData",
                new[]{
                    "Slot01",
                    "Slot02",
                    "Slot03",
                    "Slot04",
                    "Slot05",
                    "Slot06",
                    "Slot07",
                    "Slot08",
                    "Slot09",
                    "Slot10",
                    "Slot11",
                    "Slot12"
                    },
                new object[]{
                    data.CanMoveUp,
                    data.CanMoveDown,
                    data.YourLocation,
                    data.YourHp,
                    data.CanYouShoot,
                    data.OpponentHp,
                    data.OpponentLocation,
                    data.OpponentBullet.ToCustomString(),
                    data.BombAvailable,
                    data.OpponentBomb.ToCustomString(),
                    data.OpponentCanShoot,
                    data.OpponentBombAvailable
                });

            var result = new List<Choice>();

            while (db.HasRows)
            {
                var choice = new Choice();
                choice.Action = db.GetValue<Enums.Action>();
                choice.Value = db.GetValue<int>();
                result.Add(choice);
            }

            return result;
        }

        /// <summary>
        /// Stores what the AI has learned from this situation
        /// </summary>
        /// <param name="data"></param>
        /// <param name="value"></param>
        public static void Update(FrameData[] data, int value)
        {
            foreach (var item in data)
            {
                // ReSharper disable once ObjectCreationAsStatement
                new Connector("dbo.StoreData",
                    new[]{
                        "Action",
                        "Value",
                        "Slot01",
                        "Slot02",
                        "Slot03",
                        "Slot04",
                        "Slot05",
                        "Slot06",
                        "Slot07",
                        "Slot08",
                        "Slot09",
                        "Slot10",
                        "Slot11",
                        "Slot12"},
                    new object[]{
                        item.Action,
                        value,
                        item.CanMoveUp,
                        item.CanMoveDown,
                        item.YourLocation,
                        item.YourHp,
                        item.CanYouShoot,
                        item.OpponentHp,
                        item.OpponentLocation,
                        item.OpponentBullet.ToCustomString(),
                        item.BombAvailable,
                        item.OpponentBomb.ToCustomString(),
                        item.OpponentCanShoot,
                        item.OpponentBombAvailable
                    });
            }

            Iteration();
        }

        /// <summary>
        /// Updates the times the AI has played
        /// </summary>
        private static void Iteration()
        {
            // ReSharper disable once ObjectCreationAsStatement
            new Connector("dbo.IncrementIteration");
        }
    }
}
