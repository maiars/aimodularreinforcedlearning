﻿using Feed;

namespace AI
{
    /// <summary>
    /// Represents a choice by the AI
    /// </summary>
    public class Choice
    {
        /// <summary>
        /// Action
        /// </summary>
        public Enums.Action Action { get; set; }
        /// <summary>
        /// Value of the action
        /// </summary>
        public int Value { get; set; }
    }
}
