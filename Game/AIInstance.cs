﻿using AI;

namespace Game
{
    public static class AIInstance
    {
        /// <summary>
        ///     AI Instance
        /// </summary>
        public static Handle AI { get; set; }
    }
}