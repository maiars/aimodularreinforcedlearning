﻿using System.Collections.Generic;
using Feed;

namespace Game
{
    /// <summary>
    /// Represents the board where the game is being played
    /// </summary>
    public class Board
    {
        /// <summary>
        /// Initializes the board
        /// </summary>
        /// <param name="size">Board square size</param>
        /// <param name="threadSleep">How much miliseconds should the program wait after each turn.
        /// This is used to make the game actually visible to humans players.
        /// Should be 0 when AI vs VI are playing.</param>
        public Board(int size, int threadSleep)
        {
            Player1 = new Player();
            Player2 = new Player();
            Outcome = Enums.Outcome.NotFinished;
            Size = size;
            Time = 0;
            ThreadSleep = threadSleep;

            Space = new BoardSpace[Size][];

            // Initialize every BoardSpace
            for (var i = 0; i < Size; i++)
            {
                Space[i] = new BoardSpace[Size];
                for (var j = 0; j < Size; j++)
                {
                    Space[i][j] = new BoardSpace();
                }
            }

            // Place players
            Space[0][0].PlayerType = Enums.PlayerType.Human;
            Space[Size - 1][0].PlayerType = Enums.PlayerType.AI;
        }

        /// <summary>
        /// Gets the size of the board
        /// </summary>
        public int Size { get; }

        /// <summary>
        /// Stores the time that has passed, doesn't mean too much, since there is a Thread.Sleep
        /// </summary>
        public int Time { get; set; }

        /// <summary>
        /// Each space on the board
        /// </summary>
        public BoardSpace[][] Space { get; set; }

        /// <summary>
        /// If the match is still going or was won by someone (or draw)
        /// </summary>
        public Enums.Outcome Outcome { get; set; }

        /// <summary>
        /// Player 1 Values
        /// </summary>
        public Player Player1 { get; set; }

        /// <summary>
        /// Player 2 Values
        /// </summary>
        public Player Player2 { get; set; }

        /// <summary>
        /// Location of each bullet
        /// </summary>
        public List<Point> Bullets
        {
            get
            {
                var bullets = new List<Point>();
                for (var i = 0; i < Space.Length; i++)
                {
                    for (var j = 0; j < Space.Length; j++)
                    {
                        if (Space[i][j].Bullet != null)
                        {
                            bullets.Add(new Point { X = i, Y = j });
                        }
                    }
                }

                return bullets;
            }
        }

        /// <summary>
        /// Location of each bomb
        /// </summary>
        public List<Point> Bombs
        {
            get
            {
                var bombs = new List<Point>();
                for (var i = 0; i < Space.Length; i++)
                {
                    for (var j = 0; j < Space.Length; j++)
                    {
                        if (Space[i][j].Bomb != null)
                        {
                            bombs.Add(new Point { X = i, Y = j });
                        }
                    }
                }

                return bombs;
            }
        }

        /// <summary>
        /// Location of Player1 bullets
        /// </summary>
        public List<Point> Player1Bullets
        {
            get
            {
                var bullets = new List<Point>();

                foreach (var bullet in Bullets)
                {
                    if (Space[bullet.X][bullet.Y].Bullet != null && Space[bullet.X][bullet.Y].Bullet.MovingEast)
                    {
                        bullets.Add(new Point { X = bullet.X, Y = bullet.Y });
                    }
                }

                return bullets;
            }
        }

        /// <summary>
        /// Location of Player2 bullets
        /// </summary>
        public List<Point> Player2Bullets
        {
            get
            {
                var bullets = new List<Point>();

                foreach (var bullet in Bullets)
                {
                    if (Space[bullet.X][bullet.Y].Bullet != null && !Space[bullet.X][bullet.Y].Bullet.MovingEast)
                    {
                        bullets.Add(new Point { X = bullet.X, Y = bullet.Y });
                    }
                }

                return bullets;
            }
        }

        /// <summary>
        /// Location of Player1 bombs
        /// </summary>
        public List<Point> Player1Bombs
        {
            get
            {
                var bombs = new List<Point>();

                foreach (var bomb in Bombs)
                {
                    if (Space[bomb.X][bomb.Y].Bomb != null && Space[bomb.X][bomb.Y].Bomb.MovingEast)
                    {
                        bombs.Add(new Point { X = bomb.X, Y = bomb.Y });
                    }
                }

                return bombs;
            }
        }


        /// <summary>
        /// Location of Player2 bombs
        /// </summary>
        public List<Point> Player2Bombs
        {
            get
            {
                var bombs = new List<Point>();

                foreach (var bomb in Bombs)
                {
                    if (Space[bomb.X][bomb.Y].Bomb != null && !Space[bomb.X][bomb.Y].Bomb.MovingEast)
                    {
                        bombs.Add(new Point { X = bomb.X, Y = bomb.Y });
                    }
                }

                return bombs;
            }
        }

        /// <summary>
        /// Player 1 Location, Y axis
        /// </summary>
        public int Player1LocationY
        {
            get
            {
                for (var i = 0; i < Space.Length; i++)
                {
                    if (Space[0][i].PlayerType == Enums.PlayerType.Human)
                    {
                        return i;
                    }
                }

                return -1;
            }
        }

        /// <summary>
        /// Player 2 Location, Y axis
        /// </summary>
        public int Player2LocationY
        {
            get
            {
                for (var i = 0; i < Space.Length; i++)
                {
                    if (Space[Space.Length - 1][i].PlayerType == Enums.PlayerType.AI)
                    {
                        return i;
                    }
                }

                return -1;
            }
        }

        /// <summary>
        /// Time the board will sleep between turns, this is to make it visible to human players on the console
        /// </summary>
        public int ThreadSleep { get; }
    }

    /// <summary>
    /// A single space in the board matrix
    /// </summary>
    public class BoardSpace
    {
        /// <summary>
        /// Human or AI
        /// </summary>
        public Enums.PlayerType? PlayerType { get; set; }

        /// <summary>
        /// Bullets travel from side of the screen to the other.
        /// They take 25% of HP.
        /// If they collide, they dissapear.
        /// </summary>
        public Projectile Bullet { get; set; }

        /// <summary>
        /// Bombs travel just like bullets.
        /// They take 75% of HP.
        /// And they cover three lanes.
        /// </summary>
        public Projectile Bomb { get; set; }
    }

    /// <summary>
    /// A projectile, could be bullet or bomb
    /// </summary>
    public class Projectile
    {
        public Projectile(bool movingEast)
        {
            MovingEast = movingEast;
        }

        /// <summary>
        /// Where is it moving
        /// </summary>
        public bool MovingEast { get; set; }
    }

    /// <summary>
    /// Represents a player
    /// </summary>
    public class Player
    {
        public Player()
        {
            Action = Enums.Action.Idle;
            Bomb = 198;
            Hp = 100;
        }

        /// <summary>
        /// Will always be idle, except when player actually takes an action
        /// </summary>
        public Enums.Action Action { get; set; }

        /// <summary>
        /// Bomb buildup
        /// </summary>
        public int Bomb { get; set; }

        /// <summary>
        /// Hit points
        /// </summary>
        public int Hp { get; set; }
    }
}
