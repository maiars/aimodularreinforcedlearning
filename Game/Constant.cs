﻿namespace Game
{
    public static class Constant
    {
        /// <summary>
        /// How many bullets can a player have on the screen
        /// </summary>
        public static int MaximumBulletsPerPlayer = 3;

        /// <summary>
        /// How much times does it take to have a bomb ready
        /// </summary>
        public static int BombReadyAt = 100;

        /// <summary>
        /// How much time does it take to have an uber bomb ready
        /// </summary>
        public static int UberBombReadyAt = 200;

        /// <summary>
        /// Number of turns before time runs out
        /// </summary>
        public static int MaximumTime = 200;
    }
}
