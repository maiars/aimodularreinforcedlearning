﻿using System;
using System.Threading;
using AI;
using Feed;

namespace Game
{
    public static class Program
    {
        #region Properties
        /// <summary>
        /// Static instance of the board
        /// </summary>
        public static Board Board { get; set; }
        #endregion

        #region Constants
        /// <summary>
        /// If the program will fight against a human or against a VI
        /// </summary>
        private static bool humanEnemy;

        /// <summary>
        /// How many times will the iteration run
        /// </summary>
        private const int iteration = 100000;

        /// <summary>
        /// Board square size
        /// </summary>
        private const int boardSize = 7;

        /// <summary>
        /// How much miliseconds should the program wait after each turn.
        /// This is used to make the game actually visible to humans players.
        /// Should be 0 when AI vs VI are playing.
        /// </summary>
        private static int threadSleep;

        /// <summary>
        /// If the AI is training, meaning no output will be shown, except the counter.
        /// </summary>
        private static bool train;
        #endregion

        /// <summary>
        /// Main routine, to select enemy type and then run a certain amount of times
        /// </summary>
        // ReSharper disable once FunctionRecursiveOnAllPaths
        private static void Main()
        {
            Console.WriteLine("Human    |   VI  |    Train AI");
            Console.WriteLine();
            Console.WriteLine("Press H if you want to play against AI");
            Console.WriteLine("Press V to see VI play against AI");
            Console.WriteLine("Press T to train the AI (no output, just the counter)");
            bool choice;
            do
            {
                var input = Console.ReadKey();

                switch (input.Key)
                {
                    case ConsoleKey.H:
                        choice = true;
                        humanEnemy = true;
                        train = false;
                        break;
                    case ConsoleKey.V:
                        choice = true;
                        humanEnemy = false;
                        train = false;
                        break;
                    case ConsoleKey.T:
                        choice = true;
                        humanEnemy = false;
                        train = true;
                        break;
                    default:
                        choice = false;
                        break;
                }
            } while (!choice);

            // Against a human enemy
            if (humanEnemy)
            {
                threadSleep = 10;
                GameRun();
            }
            else
            {
                // Training
                if (train)
                {
                    threadSleep = 0;
                    for (var i = 0; i < iteration; i++)
                    {
                        GameRun();
                        Console.Clear();
                        Console.Write(i);
                    }
                }
                // VI against AI
                else
                {
                    threadSleep = 0;
                    GameRun();
                }
            }

            Console.Read();

            Main();
        }

        /// <summary>
        /// Routine that creates a board and runs a game
        /// </summary>
        private static void GameRun()
        {
            Board = new Board(boardSize, threadSleep);
            AIInstance.AI = new Handle();
            if (!humanEnemy)
            {
                while (Board.Outcome == Enums.Outcome.NotFinished)
                {
                    Turn(Board);
                }
            }
            else
            {
                ConsoleKeyInfo input;
                do
                {
                    #region No key pressed
                    while (!Console.KeyAvailable && Board.Outcome == Enums.Outcome.NotFinished)
                    {
                        Turn(Board);
                    }
                    #endregion

                    if (Board.Outcome != Enums.Outcome.NotFinished)
                    {
                        return;
                    }

                    #region Key pressed
                    input = Console.ReadKey(true);
                    var action = Enums.Action.Idle;

                    switch (input.Key)
                    {
                        case ConsoleKey.W:
                            action = Enums.Action.MoveUp;
                            break;
                        case ConsoleKey.S:
                            action = Enums.Action.MoveDown;
                            break;
                        case ConsoleKey.K:
                            action = Enums.Action.Shoot;
                            break;
                        case ConsoleKey.L:
                            action = Enums.Action.Bomb;
                            break;
                        case ConsoleKey.M:
                            action = Enums.Action.UberBomb;
                            break;
                    }

                    Board.Player1.Action = action;

                    Turn(Board);
                    #endregion

                } while (input.Key != ConsoleKey.Escape && Board.Outcome == Enums.Outcome.NotFinished);
            }
        }

        #region FrameData
        /// <summary>
        /// Creates an object with all the data of the frame
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        public static FrameData GetData(Board board, Enums.PlayerType player)
        {
            var data = new FrameData
            {
                #region Gen1
                Outcome = board.Outcome,
                CanMoveUp = CanMoveUp(board, player),
                CanMoveDown = CanMoveDown(board, player),
                CanYouShoot = CanShoot(board, player),
                BombAvailable = CanBomb(board, player)
                #endregion
            };

            #region Gen1
            switch (player)
            {
                case Enums.PlayerType.Human:
                    data.YourLocation = board.Player1LocationY;
                    data.YourHp = board.Player1.Hp;
                    data.OpponentLocation = board.Player2LocationY;
                    data.OpponentHp = board.Player2.Hp;
                    break;
                case Enums.PlayerType.AI:
                    data.YourLocation = board.Player2LocationY;
                    data.YourHp = board.Player2.Hp;
                    data.OpponentLocation = board.Player1LocationY;
                    data.OpponentHp = board.Player1.Hp;
                    break;
            }
            #endregion
            #region Gen2
            switch (player)
            {
                case Enums.PlayerType.Human:
                    data.OpponentBullet = board.Player2Bullets.ToArray();
                    break;
                case Enums.PlayerType.AI:
                    data.OpponentBullet = board.Player1Bullets.ToArray();
                    break;
            }
            #endregion
            #region Gen3
            switch (player)
            {
                case Enums.PlayerType.Human:
                    data.BombAvailable = CanBomb(board, Enums.PlayerType.Human);
                    data.OpponentBomb = board.Player2Bombs.ToArray();
                    break;
                case Enums.PlayerType.AI:
                    data.BombAvailable = CanBomb(board, Enums.PlayerType.AI);
                    data.OpponentBomb = board.Player1Bombs.ToArray();
                    break;
            }

            #endregion
            #region Gen4
            switch (player)
            {
                case Enums.PlayerType.Human:
                    data.OpponentCanShoot = CanShoot(board, Enums.PlayerType.AI);
                    data.OpponentBombAvailable = CanBomb(board, Enums.PlayerType.AI);
                    break;
                case Enums.PlayerType.AI:
                    data.OpponentCanShoot = CanShoot(board, Enums.PlayerType.Human);
                    data.OpponentBombAvailable = CanBomb(board, Enums.PlayerType.Human);
                    break;
            }
            #endregion

            return data;
        }
        #endregion

        #region Turn
        /// <summary>
        /// Routine to pass a turn
        /// </summary>
        /// <param name="board"></param>
        public static void Turn(Board board)
        {
            if (!humanEnemy)
            {
                Board.Player1.Action = VI.Handle.Action(GetData(board, Enums.PlayerType.Human));
            }

            Board.Player2.Action = AIInstance.AI.Action(GetData(board, Enums.PlayerType.AI));
            HandleBoard(board);
            HandlePlayerAction(board);
            PrintScreen(board);
            CheckCollition(board);

            board.Time++;
            Thread.Sleep(board.ThreadSleep);

            CheckEnd(board);
        }

        /// <summary>
        /// Handles variables update and movement of projectiles
        /// </summary>
        /// <param name="board"></param>
        private static void HandleBoard(Board board)
        {
            //Update bomb build up
            if (board.Player1.Bomb < Constant.UberBombReadyAt)
            {
                board.Player1.Bomb++;
            }

            if (board.Player2.Bomb < Constant.UberBombReadyAt)
            {
                board.Player2.Bomb++;
            }

            //Move proyectiles
            foreach (var bullet in board.Bullets)
            {
                if (board.Space[bullet.X][bullet.Y].Bullet.MovingEast)
                {
                    board.Space[bullet.X + 1][bullet.Y].Bullet = board.Space[bullet.X][bullet.Y].Bullet;
                }
                else
                {
                    board.Space[bullet.X - 1][bullet.Y].Bullet = board.Space[bullet.X][bullet.Y].Bullet;
                }

                board.Space[bullet.X][bullet.Y].Bullet = null;
            }

            //Move bombs
            foreach (var bombs in board.Bombs)
            {
                if (board.Space[bombs.X][bombs.Y].Bomb.MovingEast)
                {
                    board.Space[bombs.X + 1][bombs.Y].Bomb = board.Space[bombs.X][bombs.Y].Bomb;
                }
                else
                {
                    board.Space[bombs.X - 1][bombs.Y].Bomb = board.Space[bombs.X][bombs.Y].Bomb;
                }

                board.Space[bombs.X][bombs.Y].Bomb = null;
            }
        }

        /// <summary>
        /// Handles player action
        /// </summary>
        /// <param name="board"></param>
        private static void HandlePlayerAction(Board board)
        {
            var playerType = Enums.PlayerType.Human;

            switch (board.Player1.Action)
            {
                case Enums.Action.MoveUp:
                    PlayerMoveUp(board, playerType);
                    break;
                case Enums.Action.MoveDown:
                    PlayerMoveDown(board, playerType);
                    break;
                case Enums.Action.Shoot:
                    PlayerShoot(board, playerType);
                    break;
                case Enums.Action.Bomb:
                    PlayerBomb(board, playerType);
                    break;
                case Enums.Action.UberBomb:
                    PlayerUberBomb(board, playerType);
                    break;
            }

            playerType = Enums.PlayerType.AI;

            switch (board.Player2.Action)
            {
                case Enums.Action.MoveUp:
                    PlayerMoveUp(board, playerType);
                    break;
                case Enums.Action.MoveDown:
                    PlayerMoveDown(board, playerType);
                    break;
                case Enums.Action.Shoot:
                    PlayerShoot(board, playerType);
                    break;
                case Enums.Action.Bomb:
                    PlayerBomb(board, playerType);
                    break;
                case Enums.Action.UberBomb:
                    PlayerUberBomb(board, playerType);
                    break;
            }

            // Clear action

            board.Player1.Action = Enums.Action.Idle;
            board.Player2.Action = Enums.Action.Idle;
        }

        /// <summary>
        /// Checks collition, and handles hp
        /// </summary>
        /// <param name="board"></param>
        private static void CheckCollition(Board board)
        {
            foreach (var bullet in board.Bullets)
            {
                var space = board.Space[bullet.X][bullet.Y];

                // If bullet was removed, ignore this
                if (space.Bullet == null)
                    continue;

                // There is a bullet in the same place of a player
                if (space.PlayerType != null)
                {
                    switch (space.PlayerType)
                    {
                        case Enums.PlayerType.Human:
                            board.Player1.Hp = board.Player1.Hp - 25;
                            break;
                        case Enums.PlayerType.AI:
                            board.Player2.Hp = board.Player2.Hp - 25;
                            break;
                    }

                    // Remove it after hitting
                    space.Bullet = null;
                    continue;
                }

                // If bullet is on the border, remove it
                if (bullet.X == board.Space.Length - 1 || bullet.X == 0)
                {
                    space.Bullet = null;
                    continue;
                }

                // If bullet will collide with another bullet next turn, remove them
                if (space.Bullet.MovingEast)
                {
                    var closeSpace = board.Space[bullet.X + 1][bullet.Y];
                    if (closeSpace.Bullet != null && !closeSpace.Bullet.MovingEast)
                    {
                        space.Bullet = null;
                        closeSpace.Bullet = null;
                        continue;
                    }
                    // If it is a bomb, remove the bullet
                    if (closeSpace.Bomb != null && !closeSpace.Bomb.MovingEast)
                    {
                        space.Bullet = null;
                        continue;
                    }

                    if (bullet.X + 2 < board.Space.Length)
                    {
                        // Two bullets will occupy the same space next turn, remove them both
                        var nextCloseSpace = board.Space[bullet.X + 2][bullet.Y];
                        if (nextCloseSpace.Bullet != null && !nextCloseSpace.Bullet.MovingEast)
                        {
                            space.Bullet = null;
                            nextCloseSpace.Bullet = null;
                            continue;
                        }

                        // If it is a bomb, remove the bullet
                        if (nextCloseSpace.Bomb != null && !nextCloseSpace.Bomb.MovingEast)
                        {
                            space.Bullet = null;
                        }
                    }
                }
                else
                {
                    var closeSpace = board.Space[bullet.X - 1][bullet.Y];
                    if (closeSpace.Bullet != null && closeSpace.Bullet.MovingEast)
                    {
                        space.Bullet = null;
                        closeSpace.Bullet = null;
                        continue;
                    }
                    // If it is a bomb, remove the bullet
                    if (closeSpace.Bomb != null && closeSpace.Bomb.MovingEast)
                    {
                        space.Bullet = null;
                        continue;
                    }

                    if (bullet.X - 2 >= 0)
                    {
                        // Two bullets will occupy the same space next turn, remove them both
                        var nextCloseSpace = board.Space[bullet.X - 2][bullet.Y];
                        if (nextCloseSpace.Bullet != null && nextCloseSpace.Bullet.MovingEast)
                        {
                            space.Bullet = null;
                            nextCloseSpace.Bullet = null;
                            continue;
                        }

                        // If it is a bomb, remove the bullet
                        if (nextCloseSpace.Bomb != null && nextCloseSpace.Bomb.MovingEast)
                        {
                            space.Bullet = null;
                        }
                    }
                }
            }

            foreach (var bomb in board.Bombs)
            {
                // There is a bomb in the same place of a player
                if (board.Space[bomb.X][bomb.Y].PlayerType != null)
                {
                    if (board.Space[bomb.X][bomb.Y].PlayerType == Enums.PlayerType.Human)
                    {
                        board.Player1.Hp = board.Player1.Hp - 75;
                    }
                    if (board.Space[bomb.X][bomb.Y].PlayerType == Enums.PlayerType.AI)
                    {
                        board.Player2.Hp = board.Player2.Hp - 75;
                    }
                    // Remove it after hitting
                    board.Space[bomb.X][bomb.Y].Bomb = null;
                }

                // If bomb is on the border, remove it
                if (bomb.X == board.Space.Length - 1 || bomb.X == 0)
                {
                    board.Space[bomb.X][bomb.Y].Bomb = null;
                }
            }

            // HP can't go below 0
            if (board.Player1.Hp <= 0)
            {
                board.Player1.Hp = 0;
            }

            if (board.Player2.Hp <= 0)
            {
                board.Player2.Hp = 0;
            }
        }

        /// <summary>
        /// Checks if the match has ended
        /// </summary>
        /// <param name="board"></param>
        private static void CheckEnd(Board board)
        {
            // Check Death
            if (board.Player1.Hp <= 0 && board.Player2.Hp <= 0)
            {
                board.Outcome = Enums.Outcome.Neutral;
            }
            else if (board.Player1.Hp <= 0)
            {
                board.Outcome = Enums.Outcome.Good;
            }
            else if (board.Player2.Hp <= 0)
            {
                board.Outcome = Enums.Outcome.Bad;
            }

            // Game ended because someone died
            if (board.Outcome != Enums.Outcome.NotFinished)
            {
                PrintScreen(board);
                AIInstance.AI.IterationEnd(board.Outcome);
                return;
            }

            // Game ended because of time ran out
            if (board.Time == Constant.MaximumTime)
            {
                if (board.Player1.Hp == board.Player2.Hp)
                    board.Outcome = Enums.Outcome.Neutral;
                if (board.Player1.Hp > board.Player2.Hp)
                    board.Outcome = Enums.Outcome.Bad;
                if (board.Player1.Hp < board.Player2.Hp)
                    board.Outcome = Enums.Outcome.Good;

                PrintScreen(board);
                AIInstance.AI.IterationEnd(board.Outcome);
            }
        }
        #endregion

        #region Print
        /// <summary>
        /// Prints the screen with values and board state
        /// </summary>
        /// <param name="board"></param>
        private static void PrintScreen(Board board)
        {
            // Don't print anything if training
            if (train) return;

            Console.Clear();

            // Board printing routine

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.Write("Player 1 Hp  :");
            Console.WriteLine(PrettyPrintHP(board.Player1.Hp));
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.Write("Player 1 Bomb:");
            Console.WriteLine(PrettyPrintBomb(board.Player1.Bomb));
            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Write("Player 2 Hp  :");
            Console.WriteLine(PrettyPrintHP(board.Player2.Hp));
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.Write("Player 2 Bomb:");
            Console.WriteLine(PrettyPrintBomb(board.Player2.Bomb));
            Console.WriteLine();


            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("Time:" + (Constant.MaximumTime - board.Time));

            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;

            for (var i = 0; i < board.Space.Length; i++)
            {
                var line = "|";

                // ReSharper disable once ForCanBeConvertedToForeach
                for (var j = 0; j < board.Space.Length; j++)
                {
                    // If there is a player
                    if (board.Space[j][i].PlayerType != null)
                    {
                        // If it was hit
                        if (board.Space[j][i].Bullet != null || board.Space[j][i].Bomb != null)
                        {
                            line += " X ";
                        }
                        else
                        {
                            if (board.Player2.Hp == 0 && board.Space[j][i].PlayerType == Enums.PlayerType.AI ||
                                board.Player1.Hp == 0 && board.Space[j][i].PlayerType == Enums.PlayerType.Human)
                            {
                                line += " █ ";
                            }
                            else
                            {
                                line += " O ";
                            }
                        }
                        continue;
                    }
                    // If there is nothing
                    if (board.Space[j][i].Bullet == null && board.Space[j][i].Bomb == null)
                    {
                        line += " - ";
                        continue;
                    }

                    // If there is a bullet
                    if (board.Space[j][i].Bullet != null)
                    {
                        if (board.Space[j][i].Bullet.MovingEast)
                        {
                            line += " > ";
                        }
                        else
                        {

                            line += " < ";
                        }
                    }

                    // If there is a bomb
                    if (board.Space[j][i].Bomb != null)
                    {
                        if (board.Space[j][i].Bomb.MovingEast)
                        {
                            line += " } ";
                        }
                        else
                        {

                            line += " { ";
                        }
                    }
                }

                line += "|";
                Console.WriteLine(line);
            }

            // Printing outcome
            if (board.Outcome != Enums.Outcome.NotFinished)
            {
                var outcomeText = "";
                switch (board.Outcome)
                {
                    case Enums.Outcome.Bad:
                        outcomeText = "Player Wins.";
                        break;
                    case Enums.Outcome.Good:
                        outcomeText = "AI Wins.";
                        break;
                    case Enums.Outcome.Neutral:
                        outcomeText = "Draw.";
                        break;
                }

                Console.WriteLine(outcomeText);
            }
        }

        /// <summary>
        /// Creates a pretty string of the HP of players
        /// </summary>
        /// <param name="hp"></param>
        /// <returns></returns>
        private static string PrettyPrintHP(int hp)
        {
            const string spaces = "  ";
            Console.BackgroundColor = ConsoleColor.Black;
            switch (hp)
            {
                case 100:
                    Console.ForegroundColor = ConsoleColor.Green;
                    return spaces + "╬╬╬╬";
                case 75:
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    return spaces + " ╬╬╬";
                case 50:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    return spaces + "  ╬╬";
                case 25:
                    Console.ForegroundColor = ConsoleColor.Red;
                    return spaces + "   ╬";
                case 0:
                    Console.ForegroundColor = ConsoleColor.White;
                    return spaces + "";
                default:
                    return "?";
            }
        }

        /// <summary>
        /// Creates a pretty string of the HP of players
        /// </summary>
        /// <param name="bomb"></param>
        /// <returns></returns>
        private static string PrettyPrintBomb(int bomb)
        {
            const string spaces = "  ";
            Console.BackgroundColor = ConsoleColor.Black;

            if (bomb == Constant.UberBombReadyAt)
                return spaces + bomb + " !!";
            if (bomb >= Constant.BombReadyAt)
                return spaces + bomb + " !";
            return spaces + bomb;

        }
        #endregion

        #region Player action
        /// <summary>
        /// Can the player move up?
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        private static bool CanMoveUp(Board board, Enums.PlayerType player)
        {
            var playerX = GetPlayerCoordinateX(board, player);

            return board.Space[playerX][0].PlayerType != player;
        }

        /// <summary>
        /// Can the player move down?
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        private static bool CanMoveDown(Board board, Enums.PlayerType player)
        {
            var playerX = GetPlayerCoordinateX(board, player);

            return board.Space[playerX][board.Size - 1].PlayerType != player;
        }

        /// <summary>
        /// Move the player up
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        private static void PlayerMoveUp(Board board, Enums.PlayerType player)
        {
            var playerX = GetPlayerCoordinateX(board, player);
            int previousLocation;
            switch (player)
            {
                case Enums.PlayerType.Human:
                    previousLocation = board.Player1LocationY;
                    break;
                case Enums.PlayerType.AI:
                    previousLocation = board.Player2LocationY;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (CanMoveUp(board, player))
            {
                board.Space[playerX][previousLocation].PlayerType = null;
                board.Space[playerX][previousLocation - 1].PlayerType = player;
            }
        }

        /// <summary>
        /// Move the player down
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        private static void PlayerMoveDown(Board board, Enums.PlayerType player)
        {
            var playerX = GetPlayerCoordinateX(board, player);
            int previousLocation;
            switch (player)
            {
                case Enums.PlayerType.Human:
                    previousLocation = board.Player1LocationY;
                    break;
                case Enums.PlayerType.AI:
                    previousLocation = board.Player2LocationY;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (CanMoveDown(board, player))
            {
                board.Space[playerX][previousLocation].PlayerType = null;
                board.Space[playerX][previousLocation + 1].PlayerType = player;
            }
        }

        /// <summary>
        /// If the bullets on the screen are lower to the maximum allowed, you can shoot
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        private static bool CanShoot(Board board, Enums.PlayerType player)
        {
            int bulletsQuantity;
            switch (player)
            {
                case Enums.PlayerType.Human:
                    bulletsQuantity = board.Player1Bullets.Count;

                    break;
                case Enums.PlayerType.AI:
                    bulletsQuantity = board.Player2Bullets.Count;
                    break;
                default:
                    throw new Exception();
            }

            return bulletsQuantity < Constant.MaximumBulletsPerPlayer;
        }

        /// <summary>
        /// Routine to create a bullet projectile in front of the player
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        private static void PlayerShoot(Board board, Enums.PlayerType player)
        {
            var projectileDirection = player == Enums.PlayerType.Human;
            int projectileX;
            int playerLocationY;
            switch (player)
            {
                case Enums.PlayerType.Human:
                    projectileX = 1;
                    playerLocationY = board.Player1LocationY;

                    break;
                case Enums.PlayerType.AI:
                    projectileX = board.Size - 2;
                    playerLocationY = board.Player2LocationY;
                    break;
                default:
                    throw new Exception();
            }


            if (CanShoot(board, player))
            {
                board.Space[projectileX][playerLocationY].Bullet = new Projectile(projectileDirection);
            }
        }

        /// <summary>
        /// Can the player shoot a bomb?
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        private static bool CanBomb(Board board, Enums.PlayerType player)
        {
            switch (player)
            {
                case Enums.PlayerType.Human:
                    return board.Player1.Bomb >= Constant.BombReadyAt;
                case Enums.PlayerType.AI:
                    return board.Player2.Bomb >= Constant.BombReadyAt;
                default:
                    throw new Exception();
            }
        }

        /// <summary>
        /// Routine to create a bomb projectile in front of the player
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        private static void PlayerBomb(Board board, Enums.PlayerType player)
        {
            if (!CanBomb(board, player))
                return;

            var projectileDirection = player == Enums.PlayerType.Human;
            int projectileX;
            int playerLocationY;
            switch (player)
            {
                case Enums.PlayerType.Human:
                    projectileX = 1;
                    playerLocationY = board.Player1LocationY;
                    board.Player1.Bomb = board.Player1.Bomb - Constant.BombReadyAt;
                    break;
                case Enums.PlayerType.AI:
                    projectileX = board.Size - 2;
                    playerLocationY = board.Player2LocationY;
                    board.Player2.Bomb = board.Player2.Bomb - Constant.BombReadyAt;
                    break;
                default:
                    throw new Exception();
            }

            board.Space[projectileX][playerLocationY].Bomb = new Projectile(projectileDirection);

            // Spawn bomb above
            if (CanMoveUp(board, player))
            {
                board.Space[projectileX][playerLocationY - 1].Bomb = new Projectile(projectileDirection);
            }

            // Spawn bomb below
            if (CanMoveDown(board, player))
            {
                board.Space[projectileX][playerLocationY + 1].Bomb = new Projectile(projectileDirection);
            }
        }

        /// <summary>
        /// Routine to create an uber bomb projectile in front of the player
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        private static void PlayerUberBomb(Board board, Enums.PlayerType player)
        {
            var projectileDirection = player == Enums.PlayerType.Human;
            int projectileX;
            int playerLocationY;
            switch (player)
            {
                case Enums.PlayerType.Human:
                    if (board.Player1.Bomb == Constant.UberBombReadyAt)
                    {
                        projectileX = 1;
                        playerLocationY = board.Player1LocationY;
                        board.Player1.Bomb = 0;
                    }
                    else
                    {
                        return;
                    }
                    break;
                case Enums.PlayerType.AI:
                    if (board.Player2.Bomb == Constant.UberBombReadyAt)
                    {
                        projectileX = board.Size - 2;
                        playerLocationY = board.Player2LocationY;
                        board.Player2.Bomb = 0;
                    }
                    else
                    {
                        return;
                    }
                    break;
                default:
                    throw new Exception();
            }

            board.Space[projectileX][playerLocationY].Bomb = new Projectile(projectileDirection);

            // Spawn bomb above
            if (CanMoveUp(board, player))
            {
                board.Space[projectileX][playerLocationY - 1].Bomb = new Projectile(projectileDirection);
            }

            // Spawn bomb above the previous
            if (playerLocationY - 2 >= 0)
            {
                board.Space[projectileX][playerLocationY - 2].Bomb = new Projectile(projectileDirection);
            }

            // Spawn bomb below
            if (CanMoveDown(board, player))
            {
                board.Space[projectileX][playerLocationY + 1].Bomb = new Projectile(projectileDirection);
            }

            // Spawn bomb below the previous
            if (playerLocationY + 2 < board.Size)
            {
                board.Space[projectileX][playerLocationY + 2].Bomb = new Projectile(projectileDirection);
            }
        }
        #endregion

        #region Helper
        /// <summary>
        /// Gets the player X location according to player type.
        /// Human is always 0, AI is on the other edge
        /// </summary>
        /// <param name="board"></param>
        /// <param name="player"></param>
        /// <returns></returns>
        private static int GetPlayerCoordinateX(Board board, Enums.PlayerType player)
        {
            switch (player)
            {
                case Enums.PlayerType.Human:
                    return 0;
                case Enums.PlayerType.AI:
                    return board.Size - 1;
                default:
                    throw new Exception();
            }
        }
        #endregion
    }
}
