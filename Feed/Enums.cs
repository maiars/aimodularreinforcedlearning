﻿namespace Feed
{
    public class Enums
    {
        /// <summary>
        /// Possible outcomes from a match
        /// </summary>
        public enum Outcome
        {
            NotFinished,
            Bad,
            Good,
            Neutral
        }

        /// <summary>
        /// Possible action for a player
        /// </summary>
        public enum Action
        {
            Idle,
            MoveUp,
            MoveDown,
            Shoot,
            Bomb,
            UberBomb
        }

        /// <summary>
        /// Player Type
        /// </summary>
        public enum PlayerType
        {
            Human,
            AI
        }
    }
}
