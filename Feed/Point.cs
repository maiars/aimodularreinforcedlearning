﻿namespace Feed
{
    /// <summary>
    /// Represents a coordinate point
    /// </summary>
    public struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
