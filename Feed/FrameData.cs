﻿using System;

namespace Feed
{
    public class FrameData : IEquatable<FrameData>
    {
        #region Possibilities
        public bool CanMoveUp { get; set; }
        public bool CanMoveDown { get; set; }
        public bool CanYouShoot { get; set; }
        public bool BombAvailable { get; set; }
        #endregion

        #region Your information
        public int YourLocation { get; set; }
        public int YourHp { get; set; }
        #endregion

        #region Enemy information
        public int OpponentHp { get; set; }
        public int OpponentLocation { get; set; }
        public bool OpponentCanShoot { get; set; }
        public bool OpponentBombAvailable { get; set; }
        public Point[] OpponentBullet { get; set; }
        public Point[] OpponentBomb { get; set; }
        #endregion

        /// <summary>
        /// Outcome of the current frame
        /// </summary>
        public Enums.Outcome Outcome { get; set; }

        /// <summary>
        /// This is only filled inside the AI.
        /// The program sends it null
        /// </summary>
        public Enums.Action? Action { get; set; }

        public bool Equals(FrameData other)
        {
            if (CanMoveUp == other.CanMoveUp &&
                CanMoveDown == other.CanMoveDown &&
                YourLocation == other.YourLocation &&
                YourHp == other.YourHp &&
                CanYouShoot == other.CanYouShoot &&
                OpponentHp == other.OpponentHp &&
                OpponentLocation == other.OpponentLocation &&
                Action == other.Action)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return (CanMoveUp.ToString() +
                    CanMoveDown +
                    YourLocation +
                    YourHp +
                    CanYouShoot +
                    OpponentHp +
                    OpponentLocation +
                    Action).GetHashCode();
        }
    }
}
