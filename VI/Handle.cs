﻿using System;
using Feed;

namespace VI
{
    /// <summary>
    /// Handles Virtual Intelligence
    /// </summary>
    public static class Handle
    {
        /// <summary>
        /// Chooses an action, based on randomnes
        /// </summary>
        /// <param name="data"></param>
        public static Enums.Action Action(FrameData data)
        {
            var random = new Random().Next(0, 5);
            var action = (Enums.Action)Enum.Parse(typeof(Enums.Action), random.ToString());

            switch (action)
            {
                case Enums.Action.Idle:
                    break;
                case Enums.Action.MoveUp:
                    if (!data.CanMoveUp)
                        return Enums.Action.Idle;
                    break;
                case Enums.Action.MoveDown:
                    if (!data.CanMoveDown)
                        return Enums.Action.Idle;
                    break;
                case Enums.Action.Shoot:
                    if (!data.CanYouShoot)
                        return Enums.Action.Idle;
                    break;
                case Enums.Action.Bomb:
                    if (!data.BombAvailable)
                        return Enums.Action.Idle;
                    break;
                case Enums.Action.UberBomb:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return action;
        }
    }
}
